package ru.vsu.logic;

import ru.vsu.entity.Student;
import ru.vsu.entity.Subject;
import ru.vsu.entity.Teacher;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class TeacherService {

    /**
     * Возвращает имя и фамилию преподавателей, преподающих только один предмет
     */
    public List<String> getSingleSubjectLecturerFio(Collection<Teacher> teachers) {
        return null;
    }

    /**
     * Возвращает мапу, где ключ - имя преподавателя, а значение - студенты, для которых он является наставником
     */
    public Map<String, List<Student>> getTeacherNameToSupervisedStudentsMap(Collection<Student> students) {
        return null;
    }

    /**
     * Возвращает сумму зарплат всех преподавателей
     */
    public BigDecimal getTeachersSalarySum(Collection<Teacher> teachers) {
        return null;
    }

    /**
     * Возвращает имя и фамилию преподавателя, который может вести заданный предмет
     */
    public String findTeacherBySubject(Collection<Teacher> teachers, Subject subject) {
        return null;
    }
}
