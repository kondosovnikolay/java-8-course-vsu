package ru.vsu.logic;

import ru.vsu.entity.ExamResult;
import ru.vsu.entity.Student;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StudentService {

    /**
     * Возвращает упорядоченные фамилии совершеннолетних студентов
     */
    public List<String> getAdultStudentsLastNameSorted(Collection<Student> students) {
        return null;
    }

    /**
     * Возвращает стунентов-отличников
     */
    public Set<Student> getExcellentStudents(Collection<Student> students) {
        return null;
    }

    /**
     * Возвращает среднюю оценку
     */
    public Double getAverageMark(Collection<Student> students) {
        return null;
    }

    /**
     * Возвращает самого молодого стунендта
     */
    public Student findYoungestStudent(Collection<Student> students) {
        return null;
    }

}
